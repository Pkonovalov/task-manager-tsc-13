package ru.konovalov.tm.service;

import ru.konovalov.tm.api.IProjectRepository;
import ru.konovalov.tm.api.IProjectTaskService;
import ru.konovalov.tm.api.ITaskRepository;
import ru.konovalov.tm.model.Project;
import ru.konovalov.tm.model.Task;

import java.util.List;

public class ProjectTaskService implements IProjectTaskService {

    private IProjectRepository projectRepository;

    private ITaskRepository taskRepository;

    public ProjectTaskService( IProjectRepository projectRepository, ITaskRepository taskRepository) {
        this.taskRepository = taskRepository;
        this.projectRepository = projectRepository;
    }

    @Override
    public List<Task> findALLTaskByProjectId(final String projectId) {
        if (projectId == null || projectId.isEmpty()) return null;
        return taskRepository.findALLTaskByProjectId(projectId);
    }

    @Override
    public Task assignTaskByProjectId(final String projectId, final String taskId) {
        if (projectId == null || projectId.isEmpty() || taskId == null || taskId.isEmpty()) return null;
        return taskRepository.assignTaskByProjectId(projectId, taskId);
    }

    @Override
    public Task unassignTaskByProjectId(final String taskId) {
        if (taskId == null || taskId.isEmpty()) return null;
        return taskRepository.unassignTaskByProjectId(taskId);
    }

    @Override
    public List<Task> removeTasksByProjectId(final String projectId) {
        if (projectId == null || projectId.isEmpty()) return null;
        return taskRepository.removeAllTaskByProjectId(projectId);
    }

    @Override
    public Project removeProjectById(final String projectId) {
        if (projectId == null || projectId.isEmpty()) return null;
        if (removeTasksByProjectId(projectId) == null) return projectRepository.removeOneById(projectId);
        return null;
    }
}
