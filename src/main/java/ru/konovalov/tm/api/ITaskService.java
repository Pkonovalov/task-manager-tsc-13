package ru.konovalov.tm.api;

import ru.konovalov.tm.enumerated.Status;
import ru.konovalov.tm.model.Project;
import ru.konovalov.tm.model.Task;

import java.util.List;

public interface ITaskService {
    List<Task> findALL();

    Task add(String name, String description);

    void add(Task task);

    void remove(Task task);

    void clear();

    Task removeOneById(String id);

    Task findOneById(String id);

    Task findOneByName(String name);

    Task removeOneByName(String name);

    Task removeTaskByIndex(Integer index);

    Task findOneByIndex(Integer index);

    Task updateTaskByIndex(Integer index, String name, String description);

    Task updateTaskById(String id, String name, String description);

    Task finishTaskById(String id);

    Task finishTaskByName(String name);

    Task startTaskById(String id);

    Task startTaskByIndex(Integer index);

    Task startTaskByName(String name);

    Task changeTaskStatusById(String id, Status status);

    Task changeTaskStatusByName(String name, Status status);

    Task changeTaskStatusByIndex(Integer index, Status status);
}
